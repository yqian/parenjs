// (C) 2013 KIM Taegyoon
// Paren language core
// Paren-to-JavaScript compiler

paren = (function() {
  var VERSION="0.2.3";

  // detect browser
  if (typeof(window) === "undefined") { // node.js?
    var write=function(a){process.stdout.write(String(a));};
    var writeln=console.log;
  } else { // in browser
    var write=function(a){document.getElementsByTagName("body")[0].innerHTML+=a;}
    var writeln=function(a){document.getElementsByTagName("body")[0].innerHTML+=a+"<br/>";}
  }

  function tokenizer(s) {
    var ret = [];
    var acc = ""; // accumulator

    function emit() {
        if (acc.length > 0) {ret.push(acc); acc = "";}
    }

    this.tokenize = function() {
      var last = s.length - 1;
      var unclosed = 0;
      for (var pos=0; pos <= last; pos++) {
          var c = s.charAt(pos);
          if (c == ' ' || c == '\t' || c == '\r' || c == '\n') {
              emit();
          }
          else if (c == ';') { // end-of-line comment
              emit();
              do pos++; while (pos <= last && s.charAt(pos) != '\n');
          }
          else if (c == '"') { // beginning of string
            unclosed++;
            emit();
            acc += c;
            pos++;
            while (pos <= last) {
              c = s.charAt(pos);
              if (c == '"') {unclosed--; acc += c; break;}
              if (c == '\\') { // escape
                var next = s.charAt(pos+1);
                if (next == 'r') next = '\r';
                else if (next == 'n') next = '\n';
                else if (next == 't') next = '\t';
                acc += next;
                pos += 2;
              }
              else {
                acc += c;
                pos++;
              }
            }
            emit();
          }
          else if (c == '(') {
            unclosed++;
            emit();
            acc += c;
            emit();
          }
          else if (c == ')') {
            unclosed--;
            emit();
            acc += c;
            emit();
          }
          else {
            acc += c;
          }
      }
      emit();
      if (unclosed != 0) throw new Error("Unclosed: " + unclosed);
      return ret;
    };
  }
  
  function tokenize(s) {return (new tokenizer(s)).tokenize();}

  function parser(tokens) {
    var pos = 0;

    this.parse = function() {
      var ret = [];
      var last = tokens.length - 1;
      for (;pos <= last; pos++) {
        var tok = tokens[pos];
        if (tok.charAt(0) == '"') { // double-quoted string
          ret.push(tok);
        }
        else if (tok == "(") { // list
          pos++;
          ret.push(this.parse());
        }
        else if (tok == ")") { // end of list
          break;
        }
        else if (!isNaN(Number(tok))) { // number
          ret.push(Number(tok));
        }
        else { // symbol
          ret.push(tok);
        }
      }
      return ret;
    };
  };

  function parse(s) {return (new parser(tokenize(s))).parse();}

  function parenthesize(s) {return "(" + s + ")";}
  
  function fn(s) {return parenthesize("function(){"+s+"}");}
  
  // built-in functions
  function inc(a) {
    return a+1}
  
  function dec(a) {
    return a-1}
  
  function list() {
    return arguments;
  }
  
  function apply(f, lst) {
    var code="f(";
    for (var i=0; i<lst.length; i++) {
      if (i>0) code+=",";
      code+=lst[i];
    }
    code+=")";
    return eval(code);
  }
  
  function fold(f, lst) {
    var acc=lst[0];
    for (var i=1; i<lst.length; i++) {
      acc=f(acc,lst[i]);
    }
    return acc;
  }  
  
  function map(f, lst) {
    var ret=[];
    for (var i=0; i<lst.length; i++){
      ret.push(f(lst[i]));}
    return ret;
  }
  
  function filter(f, lst) {
    var ret=[];
    for (var i=0; i<lst.length; i++){
      var a=f(lst[i]);
      if (a) ret.push(lst[i]);}
    return ret;
  }  

  function range(start, end, step) {
    var ret=[];
    if (step>=0) {
      for(var i=start; i<=end; i+=step){
        ret.push(i);
      }
      return ret;}
    else {
      for(var i=start; i>=end; i+=step){
        ret.push(i);
      }
    return ret;} 
  }
  
  function nth(index, lst) {
    return lst[index];
  }
  
  function length(lst) {
    return lst.length;
  }
  
  function pr() {
    var acc="";
    for (var i=0; i<arguments.length; i++) {
      if(i>0) acc+=" ";
      acc+=arguments[i];
    }
    write(acc);
  }
  
  function prn() {
    var acc="";
    for (var i=0; i<arguments.length; i++) {
      if(i>0) acc+=" ";
      acc+=arguments[i];
    }
    writeln(acc);
  }
  
  function compile(n) {
    if(n instanceof Array) { // function (FUNCTION ARGUMENT ..)
      var func = compile(n[0]);
      switch(func) {
      case "++": // (++ X)
          return parenthesize("++" + compile(n[1]));
      case "--": // (-- X)
          return parenthesize("--" + compile(n[1]));        
      case "def": // (def SYMBOL VALUE)
          return "var " + n[1] + "=" + compile(n[2]) + ";";
      case "set": // (set A VALUE)
            return parenthesize(compile(n[1]) + "=" + compile(n[2]));
      case "if": // (if CONDITION THEN_EXPR ELSE_EXPR)
          return parenthesize(compile(n[1]) + "?" + compile(n[2]) + ":" + compile(n[3]));
      case "when": // (when CONDITION EXPR ..)
          return parenthesize(compile(n[1]) + "?" + compile(n[2]) + ":null");
      case "for": // (for SYMBOL START END STEP EXPR ..)
          var sym = n[1];
          var start = compile(n[2]);
          var len = n.length;
          var last = compile(n[3]);
          var step = compile(n[4]);
          var ret = "_last="+last+";\n"+
            "_step="+step+";\n" +
            "if (_step >= 0) {\n" +
            "for("+sym+"="+start+";"+sym+"<="+last+";"+sym+"+=_step){\n";
          for (var i=5; i<len; i++) {
            ret += compile(n[i])+";\n";
          }
          ret += "}}\n" +
            "else{\n" +
            "for("+sym+"="+start+";"+sym+">="+last+";"+sym+"+=_step){\n";
          for (var i=5; i<len; i++) {
            ret += compile(n[i])+";\n";
          }
          ret += "}}";
          return ret;
      case "while": // (while CONDITION EXPR ..)
          var cond = compile(n[1]);
          var len = n.length;
          var ret = "while(" + cond + "){\n";
          for (var i = 2; i < len; i++) {
              ret+=compile(n[i])+";\n";
          }
          ret+="}";
          return ret;
      case "quote": // (quote X)
          var s=n[1].toString();
          if (s[0] == "[") return n[1]; else return "["+n[1]+"]";
      case "fn": // (fn (ARGUMENT ..) BODY) => lexical closure
        var ret = "function(";
        var args = n[1];
        ret += args.join(",")+"){\n";
        for(var i=2;i<n.length-1;i++) {
          ret+=compile(n[i])+";\n";
        }
        ret+="return " + compile(n[n.length-1])+";";
        ret+="}";
        return parenthesize(ret);
      case "begin": // (begin X ..)
        var last = n.length - 1;
        if (last <= 0) return "";
        var ret="";
        for (var i = 1; i <= last; i++) {
            ret+=compile(n[i])+";";
        }
        return ret;
      case "new":
          // (new CLASS ARG ..) ; create new object
          // => new CLASS(ARG,..)
          var ret = "new " + n[1] + "(";
          var n2 = n.slice(2);
          for(var i=0;i<n2.length;i++) {
            n2[i]=compile(n2[i]);
          }
          ret+=n2.join(",")+")";
          return parenthesize(ret);
      case ".": // (. Math floor 1.5)
        var ret = compile(n[1])+"." + n[2] + "(";
        var n2 = n.slice(3);
        for(var i=0;i<n2.length;i++) {
          n2[i]=compile(n2[i]);
        }
        ret+=n2.join(",")+")";
        return ret;
      case "[]": // ([] "abc" "length") ; access field
        return compile(n[1])+"["+compile(n[2])+"]";
      case ".get": // (.get "abc" length) ; access field
        return compile(n[1])+"."+n[2];
      default: // (func ARGUMENT ..) ; general function
          var ret = func+"(";
          var n2 = n.slice(1);
          for(var i=0;i<n2.length;i++) {
            n2[i]=compile(n2[i]);
          }
          ret+=n2.join(",")+")";
          return ret;
      } // end switch
    }
    else {
      switch(n) { // compile built-in function
      case "+": // (+ X ..)
      case "-":
      case "*":
      case "/":
        var ret=fn('var _acc=arguments[0];\n'+
          'for(var _i=1;_i<arguments.length;_i++){\n'+
          ' _acc'+n+'=arguments[_i];}\n'+
          'return _acc;');
        return ret;
      case "%": // (% DIVIDEND DIVISOR)
      case "==":
      case "!=":
      case "===":
      case "!==":
      case "<":
      case ">":
      case "<=":
      case ">=":
      case "&&":
      case "||":
          return fn('return arguments[0]'+n+'arguments[1];');
      case "!": // (! X)
          return fn('return !arguments[0];');
      case "sqrt": // (sqrt X)
      case "floor": // (floor X)
      case "ceil": // (ceil X)
      case "log": // (log X)
        return "Math." + n;
      case "^": // (^ BASE EXPONENT)
        return "Math.pow";
      case "rand": // (rand)
        return "Math.random";
      case "strlen": // (strlen X)
        return fn('return arguments[0].length;');
      case "strcat": // (strcat X ..)
        var ret=fn('var _acc=String(arguments[0]);\n'+
          'for(_i=1;_i<arguments.length;_i++){\n'+
          ' _acc+=arguments[_i];}\n'+
          'return _acc;');
        return ret;
      case "char-at": // (char-at X POSITION)
        return fn('return arguments[0].charAt(arguments[1]);');
      case "chr": // (chr X)
        return "String.fromCharCode";
      case "string": // (string X)
          return "String";
      case "double": // (double X)
          return "parseFloat";
      case "int": // (int X)
          return "parseInt";
      case "read-string": // (read-string X)
          return "paren.parse";
      case "type": // (type X)
          return "typeof";
      case "eval": // (eval X)
          return "paren.eval_sexp";
      case "inc": // (inc X)
      case "dec": // (dec X)
      case "list": // (list X ..)
      case "apply": // (apply FUNC LIST)
      case "fold": // (fold FUNC LIST)
      case "map": // (map FUNC LIST)
      case "filter": // (filter FUNC LIST)
      case "range": // (range START END STEP)
      case "nth": // (nth INDEX LIST)
      case "length": // (length LIST)
      case "pr": // (pr X ..)
      case "prn": // (prn X ..)
          return "paren." + n;
      default:
        return n;
      }
    }
  }

  function compile_all(exprs) {
    ret = "";
    for (var i=0; i<exprs.length; i++) {
      ret += compile(exprs[i]) + ";\n";
    }
    return ret;
  }

  function compile_string(s) {
    return compile_all(parse(s));
  }

  function eval_string(s) {
    return eval(compile_string(s));
  }
  
  function eval_sexp(s) {
    return eval(compile_sexp(s));
  }
  
  function compile_sexp(s) {
    return compile_all(s);
  }  

  return {VERSION:VERSION, eval:eval_string, eval_sexp:eval_sexp, compile:compile_string, compile_sexp:compile_sexp,
    tokenize:tokenize, parse:parse, pr:pr, prn:prn,
    inc:inc, dec:dec, list:list, apply:apply, fold:fold, map:map, filter:filter, range:range, nth:nth, length:length, pr:pr, prn:prn};
})();
