# Parenjs: The Paren Programming Language written in JavaScript #

(C) 2013 KIM Taegyoon

Paren is a dialect of Lisp. It is designed to be an embedded language. You can use JavaScript in your Paren program.

Parenjs compiles Paren code to JavaScript and evaluates it.

* paren.js is required to run generated JavaScript code.
* Symbol (variable) names should be valid JavaScript variable names.

Try Paren here: [http://tryparen.tistory.com/](http://tryparen.tistory.com/)

Try JavaScript here: [http://tryjs.tistory.com/](http://tryjs.tistory.com/)

## Usage (Try Paren in a web browser) ##
Open parenjs.html.

## Usage (in HTML) ##
```
<script src="paren.js"></script>
<script>
paren.compile(str); // return Paren program compiled to JavaScript
paren.eval(str); // evaluate Paren program
</script>
```

## Reference ##
```
Functions:
 ! != % && * + ++ - -- . .get
 [] / < <= = == === > >= ^
 apply begin ceil char-at chr dec def double eval exit filter
 floor fn for if inc int length list log
 map new nth pr prn quote rand range read-string set
 sqrt strcat string strlen type when while ||
Etc.:
 (list) "string" ; end-of-line comment
```

## File ##
* paren.js: Paren language library
* parenjs.html: Try Paren in a web browser

## Examples ##
### Hello, World! ###
```
(prn "Hello, World!")
```

### Function ###

In a function, [lexical scoping](http://en.wikipedia.org/wiki/Lexical_scoping#Lexical_scoping) is used.

```
> ((fn (x y) (+ x y)) 1 2)
3
> ((fn (x) (* x 2)) 3)
6
> (def sum (fn (x y) (+ x y))) ; (def a b) => var a = b;
undefined
> (sum 1 2)
3
> (fold sum (range 1 10 1))
55
> (def evenp (fn (x) (== 0 (% x 2))))
undefined
> (evenp 3)
false
> (evenp 4)
true
> (apply + (list 1 2 3))
6
> (map sqrt (list 1 2 3 4))
[ 1, 1.4142135623730951, 1.7320508075688772, 2 ]
> (filter evenp (list 1 2 3 4 5))
[ 2, 4 ]
> (== "abc" "abc")
true
> (set x 1) ; (set a b) => a = b; // global variable
  ((fn (x) (prn x) (set x 3) (prn x)) 4) ; lexical scoping
  x
4
3
1
> (set adder (fn (amount) (fn (x) (+ x amount)))) ; lexical scoping
  (set add3 (adder 3))
  (add3 4)
7
```

#### Recursion ####
```
> (set factorial (fn (x) (if (<= x 1) x (* x (factorial (dec x))))))
  (for i 1 5 1 (prn i (factorial i)))
1 1
2 2
3 6
4 24
5 120
undefined
```

### List ###
```
> (nth 1 (list 2 4 6))
4
> (length (list 1 2 3))
3
```

### JavaScript interoperability (from Paren) ###
```
> (. Math random)
> (Math.random) ; class's static method
0.4780254852371699
> (. Math floor 1.5)
> (Math.floor 1.5)
1
> ([] "abc" "length") ; access field
3
> (def a (new Array))
  (. a push 3)
  (. a push 4)
  a
3,4
```

### JavaScript interoperability (from JavaScript) ###
JavaScript
```
paren.eval("(set a 3)"); // global variable
console.log(a);
```
=> 3

### [Project Euler Problem 1](http://projecteuler.net/problem=1) ###
```
(set s 0)
(for i 1 999 1
    (when (|| (== 0 (% i 3)) (== 0 (% i 5)))
        (set s (+ s i))))
s
```
=> 233168

```
(apply + (filter (fn (x) (|| (== 0 (% x 3)) (== 0 (% x 5)))) (range 1 999 1)))
```
=> 233168

### [Project Euler Problem 2](http://projecteuler.net/problem=2) ###
```
(set a 1)
(set b 1)
(set sum 0)
(while (<= a 4000000)
  (set c (+ a b))
  (set a b)
  (set b c)
  (when (== 0 (% a 2))
    (set sum (+ sum a))))
sum
```
=> 4613732

### [Project Euler Problem 4](http://projecteuler.net/problem=4) ###
```
(set maxP 0)
(for i 100 999 1
  (for j 100 999 1
    (set p (* i j))
    (set ps (string p))
    (set len (strlen ps))
    (set to (/ len 2))
    (set pal true)
    (set k 0)
    (set k2 (dec len))
    (while
      (&& (< k to) pal)
	  (when (!= (char-at ps k) (char-at ps k2))
		(set pal false))
	  (++ k)
	  (-- k2))
	(when pal
	  (when (> p maxP)
		(set maxP p)))))
maxP
```
=> 906609

[More solutions of Project Euler in Paren](https://bitbucket.org/ktg/euler-paren)

### [99 Bottles of Beer](http://en.wikipedia.org/wiki/99_Bottles_of_Beer) ###
```
(for i 99 1 -1
  (prn i "bottles of beer on the wall," i "bottles of beer.")
  (prn "Take one down and pass it around," (dec i) "bottle of beer on the wall."))
```

## Alternative Implementations ##
* [Paren](https://bitbucket.org/ktg/paren) (Paren running natively)
* [Parenj](https://bitbucket.org/ktg/parenj) (Paren running on the Java Virtual Machine)
* [Parenjs](https://bitbucket.org/ktg/parenjs) (Paren compiler targeting JavaScript)

## License ##

   Copyright 2013 KIM Taegyoon

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
